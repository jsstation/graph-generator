const app = require('express')();
const cors = require('cors')({origin: true});
const svgHelper = require('./draw/draw.js');

app.get('/svg', (req, res) => {
  cors(req,res, () => {
    res.setHeader('Content-Type', 'image/svg+xml');
    if(req.query.tree)
      return res.send(svgHelper.drawTree(req.query.tree));
    if(req.query.ll)
      return res.send(svgHelper.drawLL(req.query.ll));
  });
});

app.listen(process.env.PORT || 3000);

