# Graph Generator


## Usage Instructions

### Generate a Linked List Image
1. Open your browser console.
2. Code out your linked list in the console. For example:

    ```js
    const c = {v: 20}
    const b = {v: 10, next: c}
    const a = {v: 5, next: b}

    ```
3. Stringify your linked list:

    ```js
    JSON.stringify(a);
    //Output: "{"v":5,"next":{"v":10,"next":{"v":20}}}"
    ```
4. Copy the stringified linked list without the outer parenthesis to the end of the URL:

    ```js
    https://jsstation.com/draw?ll={"v":5,"next":{"v":10,"next":{"v":20}}}
    ```
5. Paste URL into browser to see your linked list
    
    ![alt text](https://jsstation.com/draw?ll={%22v%22:5,%22next%22:{%22v%22:10,%22next%22:{%22v%22:20}}})
    
6. Include the image in your code.

    ```js
    <img src="https://jsstation.com/draw?ll={"v":5,"next":{"v":10,"next":{"v":20}}}"/>
    ```

### Generate a Tree Image
------
1. Open your browser console.
2. Code out your tree in the console. For example:

    ```js
    const d = {v: 25}
    const c = {v: 20, children: [d]}
    const b = {v: 10}
    const a = {v: 5, children: [b, c]}
    ```
3. Stringify your tree:

    ```js
    JSON.stringify(a);
    //Output: "{"v":5,"children":[{"v":10},{"v":20,"children":[{"v":25}]}]}"
    ```
4. Copy the stringified tree without the outer parenthesis to the end of the URL:

    ```js
    https://jsstation.com/draw?tree={"v":5,"children":[{"v":10},{"v":20,"children":[{"v":25}]}]}
    ```
5. Paste URL into browser to see your tree image

    ![alt text] (https://jsstation.com/draw?tree={%22v%22:5,%22children%22:[{%22v%22:10},{%22v%22:20,%22children%22:[{%22v%22:25}]}]})
    
6. Include the image in your code.
    ```js
    <img src="https://jsstation.com/draw?ll={"v":5,"next":{"v":10,"next":{"v":20}}}"/>
    ```

