const functions = require('firebase-functions');
const svgHelper = require('./draw/draw.js');
const cors = require('cors')({origin: true});

exports.makeGraph = functions.https.onRequest((req, res) => {
  cors(req,res, () => {
    res.setHeader('Content-Type', 'image/svg+xml');
    if(req.query.tree)
      return res.send(svgHelper.drawTree(req.query.tree));
    if(req.query.ll)
      return res.send(svgHelper.drawLL(req.query.ll));
  });
});
